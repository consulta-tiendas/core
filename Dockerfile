FROM python:3.10.5-slim

WORKDIR /usr/src

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY src/ .

CMD ["python", "./app.py"]
