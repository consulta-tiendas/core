PROJECT_NAME=consulta-tiendas-core
DOCKER_CONTAINER=$(PROJECT_NAME)
DOCKER_IMAGE=$(PROJECT_NAME)-image
DOCKER_EXEC=docker exec $(DOCKER_CONTAINER)
PYTHON=python
PIP=pip
VENV=.venv/bin/activate
TEST_PATH=tests/
PROJECT_SRC=src/

install-local:
	@echo "Installing Locally..."
	$(PYTHON) -m venv .venv
	. $(VENV)
	$(PIP) install -r requirements.txt

remove-unused:
	@echo "Remove unused..."
	autoflake --recursive --in-place --remove-all-unused-imports --remove-unused-variables $(PROJECT_SRC)

sort:
	@echo "Sorting..."
	$(DOCKER_EXEC) isort $(PROJECT_SRC)

format:
	@echo "Blacking..."
	$(DOCKER_EXEC) black $(PROJECT_SRC)

prettify: sort remove-unused format

run-local:
	@echo "Running..."
	cd $(PROJECT_SRC); \
	$(PYTHON) app.py

run:
	@echo "Docker..."
	docker exec -i $(DOCKER_CONTAINER) make run-local

.PHONY: tests

tests:
	@echo "Testing..."
	$(DOCKER_EXEC) pytest tests

coverage:
	@echo "Coverage..."
	$(DOCKER_EXEC) pytest --cov=src tests/

clean:
	@echo "Clean..."
	$(DOCKER_EXEC) find . -type d -name  "__pycache__" -exec rm -r {} +
	$(DOCKER_EXEC) rm -rf .pytest_cache
	$(DOCKER_EXEC) rm .coverage || true
	$(DOCKER_EXEC) rm index.spec || true

nuke: clean
	@echo "Nuke..."
	$(DOCKER_EXEC) rm -rf .venv
	$(DOCKER_EXEC) rm -rf build
	$(DOCKER_EXEC) rm -rf dist

docker-build:
	@echo "Docker Build"
	DOCKER_BUILDKIT=1 docker build -t $(DOCKER_IMAGE) .

docker-run:
	docker run --name $(DOCKER_CONTAINER) --rm -it -v ${PWD}/src:/usr/src -d $(DOCKER_IMAGE)
